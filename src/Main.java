
public class Main {
    public static void main(String[] args){
        Sentence first = new Sentence("Hello KSHRD!");
        Sentence second = new Sentence("*************************************");
        Sentence third = new Sentence("I will try my best to be here at HRD.");
        Sentence fouth = new Sentence("-------------------------------------");
        Sentence fifth = new Sentence("Downloading...........Completed 100%!");
        first.run();
        second.run();
        third.run();
        fouth.run();
        fifth.runFast();
    }
}
