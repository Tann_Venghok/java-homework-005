public class Sentence implements Runnable{
    public String sentence;

    public Sentence(String sentence){
        this.sentence = sentence;
    }

    public void run(){
        for(int i = 0; i < sentence.length(); i++){
            try
            {
                Thread.sleep(250);
            }
            catch(InterruptedException e)
            {
                System.out.println(e);
            }
            System.out.print(sentence.charAt(i));
        }
        System.out.println();
    }

    public void runFast(){
        for(int i = 0; i < sentence.length(); i++){
            if(i < 20) {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
                System.out.print(sentence.charAt(i));
            }else {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
                System.out.print(sentence.charAt(i));
            }
        }
        System.out.println();
    }
}
